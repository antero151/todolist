from django.contrib import admin
from .models import ToDoTask, ToDoList
# Register your models here.

admin.site.register(ToDoList)
admin.site.register(ToDoTask)