from django.db import models


class CreateUpdateMixin(models.Model):
    cdate = models.DateTimeField(auto_now_add=True)
    udate = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class ToDoList(CreateUpdateMixin):
    name = models.CharField(max_length=150)

    class Meta:
        ordering = ['cdate']

    def __str__(self):
        return self.name


class ToDoTask(CreateUpdateMixin):
    todolist = models.ForeignKey('ToDoList', related_name='todos', on_delete=models.CASCADE)
    text = models.TextField()
    done = models.BooleanField(default=False, blank=True)
    due_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['cdate']

    def __str__(self):
        return self.text