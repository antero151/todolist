from rest_framework import serializers
from .models import ToDoTask, ToDoList


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDoTask
        fields = ('id', 'text', 'done', 'udate', 'due_date', 'todolist')


class TodoListSerializer(serializers.ModelSerializer):
    todos = TaskSerializer(many=True, required=False)

    class Meta:
        model = ToDoList
        fields = ('id', 'name', 'todos')