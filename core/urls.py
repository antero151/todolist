from django.urls import path
from .views import CreateTaskView, UpdateTaskView, TaskListView, DeleteTaskView, TodoListsView, CreateTodoListView, UpdateTodoListView, DeleteTodoListView

urlpatterns = [
    path('tasks/', TaskListView.as_view(), name="task-list"),
    path('create-task/', CreateTaskView.as_view(), name="create-task"),
    path('update-task/<pk>/', UpdateTaskView.as_view(), name="update-task"),
    path('delete-task/<pk>/', DeleteTaskView.as_view(), name="delete-task"),
    path('todolists/', TodoListsView.as_view(), name='todo-lists'),
    path('create-todolist/', CreateTodoListView.as_view(), name='todo-lists'),
    path('update-todolist/<pk>/', UpdateTodoListView.as_view(), name='todo-lists'),
    path('delete-todolist/<pk>/', DeleteTodoListView.as_view(), name='todo-lists'),
]