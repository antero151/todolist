from django.shortcuts import render

# Create your views here.
from rest_framework.generics import CreateAPIView, UpdateAPIView, ListAPIView, DestroyAPIView
from .serializers import TaskSerializer, TodoListSerializer
from .models import ToDoTask, ToDoList


class CreateTaskView(CreateAPIView):
    serializer_class = TaskSerializer


class UpdateTaskView(UpdateAPIView):
    serializer_class = TaskSerializer
    queryset = ToDoTask.objects.all()


class DeleteTaskView(DestroyAPIView):
    serializer_class = TaskSerializer
    queryset = ToDoTask.objects.all()


class TaskListView(ListAPIView):
    serializer_class = TaskSerializer
    queryset = ToDoTask.objects.all()


class TodoListsView(ListAPIView):
    serializer_class = TodoListSerializer
    queryset = ToDoList.objects.all()


class CreateTodoListView(CreateAPIView):
    serializer_class = TodoListSerializer


class UpdateTodoListView(UpdateAPIView):
    serializer_class = TodoListSerializer
    queryset = ToDoList.objects.all()


class DeleteTodoListView(DestroyAPIView):
    serializer_class = TodoListSerializer
    queryset = ToDoList.objects.all()