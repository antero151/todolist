#!/usr/bin/env bash
docker-compose run server python manage.py migrate
docker-compose run client npm i
docker-compose up --build